 # Tech & Wine OBS

 ## Prerequisites
 
OBS with move plugin.

 ## Install
 
- Change `{{TECHWINE_PATH}}` by the absolute path to this directory from `techwine.json`.
- Import the `techwine.json` in OBS Scenes.
- Configure devices in `sources` section (especially `screen`, `camera 1` and `sound`.

## Use

Check your global parameters from your profile to record with good quality and remove extra audio devices if present.
